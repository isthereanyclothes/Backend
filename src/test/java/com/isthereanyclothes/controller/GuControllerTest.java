package com.isthereanyclothes.controller;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GuController.class)
@AutoConfigureMockMvc
public class GuControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testAuthorReturnExpectedString() throws Exception {
        this.mvc.perform(get("/author"))
                .andExpect(status().isOk())
                .andExpect(content().string("tim"));
    }
}