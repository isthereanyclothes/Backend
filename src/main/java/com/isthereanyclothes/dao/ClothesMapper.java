package com.isthereanyclothes.dao;

import com.isthereanyclothes.entity.Clothes;
import com.isthereanyclothes.entity.ClothesPriceLog;
import com.isthereanyclothes.entity.ClothesIndex;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ClothesMapper {

    /**
     * 以產品編號來搜尋產品資訊
     */
    Clothes getProdInfo(@Param("prod_number") Long prodNumber, @Param("prod_table_name") String prodTableName);

    /**
     * 以產品編號來搜尋產品歷史紀錄
     */
    List<ClothesPriceLog> getProdPriceLogs(@Param("prod_number") Long prodNumber, @Param("prod_table_name") String prodTableName);

    /**
     * 以姓別來搜尋所有的資料(30天內更新)
     */
    List<ClothesIndex> getAllProd(@Param("sex") int sex, @Param("prod_table_name") String prodTableName
            , @Param("log_table_name") String logTableName);

    /**
     * 以姓別來搜尋所有特別條件的資料(30天內更新)
     */
    List<ClothesIndex> getAllSP(@Param("sex") int sex, @Param("prod_table_name") String prodTableName
            , @Param("log_table_name") String logTableName, @Param("sp_condition") String spCondition);
}