package com.isthereanyclothes.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isthereanyclothes.entity.Clothes;
import com.isthereanyclothes.entity.ClothesPriceLog;
import com.isthereanyclothes.entity.ClothesIndex;
import com.isthereanyclothes.entity.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClothesService {
    @Autowired
    private com.isthereanyclothes.dao.ClothesMapper ClothesMapper;

    /**
     * 以產品編號來搜尋(30天內的紀錄)
     */
    public String getProdALLInfo(Long prodNumber, String prodTableName) throws JsonProcessingException {
        Clothes clothes = ClothesMapper.getProdInfo(prodNumber, prodTableName);
        if (clothes == null) {
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.prodnNotExists();
            return FormatJson;
        } else {
            String logTableName = prodTableName + "_price_log";
            List<ClothesPriceLog> clothePriceLogs = ClothesMapper.getProdPriceLogs(prodNumber, logTableName);
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.prodALLInfo(clothes, clothePriceLogs);
            return FormatJson;
        }
    }

    /**
     * 查詢所有單一性別資料(30天內有更新)
     */
    public String getAllProd(int sex, String prodTableName) throws JsonProcessingException {
        String logTableName = prodTableName + "_price_log";
        List<ClothesIndex> allProdIndex = ClothesMapper.getAllProd(sex, prodTableName, logTableName);
        if (allProdIndex.size() > 0) {
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.prodAllIndex(allProdIndex);
            return FormatJson;
        } else {
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.indexEmpty();
            return FormatJson;
        }
    }

    /**
     * 查詢所有單一性別特殊條件資料(30天內有更新)
     */
    public String getAllSP(int sex, String prodTableName, String spCondition) throws JsonProcessingException {
        String logTableName = prodTableName + "_price_log";
        List<ClothesIndex> allProdIndex = ClothesMapper.getAllSP(sex, prodTableName, logTableName, spCondition);
        if (allProdIndex.size() > 0) {
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.prodAllIndex(allProdIndex);
            return FormatJson;
        } else {
            JsonFormat jsonFormat = new JsonFormat();
            String FormatJson = jsonFormat.indexEmpty();
            return FormatJson;
        }
    }
}

