package com.isthereanyclothes.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Parameter;

@Aspect
@Component
public class LogAspect {

    //Pointcut：要被AOP切入的位置，使用pointcut expression來表示
    //Pointcut位置的Join point即為Advice施行的目標
    //下面的@Pointcut("execution(* idv.matt.controller..*(..))")即表示切入位置為idv.matt.controller下的任意method。
    @Pointcut("execution(* com.isthereanyclothes.controller..*(..))")
    public void pointcut() {
    }

    @Before("pointcut()")
    public void before(JoinPoint joinPoint) {
        String className = joinPoint.getTarget().getClass().getName();// 取得切入點的類別名稱
        String methodName = joinPoint.getSignature().getName(); // 取得切入點的方法名稱
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Parameter[] parameters = signature.getMethod().getParameters(); // 取得方法輸入參數資訊
        Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass().getName());
        Object[] args = joinPoint.getArgs(); // 取得輸入參數值
        logger.info("className:" + className + " methodName:" + methodName
                + " paramInfo" + parameters + " param" + args);
    }


}
