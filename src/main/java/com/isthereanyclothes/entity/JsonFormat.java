package com.isthereanyclothes.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class JsonFormat {
    private static ObjectMapper objMapper;

    public static String Error() throws JsonProcessingException {
        objMapper = new ObjectMapper();
        Map<String, Object> respMap = new LinkedHashMap<>();
        Map<String, Object> respInfoMap = new LinkedHashMap<>();
        Map<String, Object> responseErroInfo = new LinkedHashMap<>();
        responseErroInfo.put("resultCode", 404);
        responseErroInfo.put("message", "Error");
        respInfoMap.put("responseErroInfo", responseErroInfo);
        respMap.put("error", respInfoMap);
        String jsonString = objMapper.writeValueAsString(respMap);
        return jsonString;
    }


    public static String prodnNotExists() throws JsonProcessingException {
        objMapper = new ObjectMapper();
        Map<String, Object> respMap = new LinkedHashMap<>();
        Map<String, Object> respInfoMap = new LinkedHashMap<>();
        Map<String, Object> responseErroInfo = new LinkedHashMap<>();
        Map<String, Object> responseErroDetail = new LinkedHashMap<>();
        responseErroDetail.put("resultCode", 501);
        responseErroDetail.put("message", "查無此商品資訊");
        responseErroInfo.put("resultCode", 100);
        responseErroInfo.put("message", "error");
        responseErroInfo.put("message", responseErroDetail);
        respInfoMap.put("responseErroInfo", responseErroInfo);
        respMap.put("error", respInfoMap);
        String jsonString = objMapper.writeValueAsString(respMap);
        return jsonString;
    }

    public static String indexEmpty() throws JsonProcessingException {
        objMapper = new ObjectMapper();
        Map<String, Object> respMap = new LinkedHashMap<>();
        Map<String, Object> respInfoMap = new LinkedHashMap<>();
        Map<String, Object> responseErroInfo = new LinkedHashMap<>();
        Map<String, Object> responseErroDetail = new LinkedHashMap<>();
        responseErroDetail.put("resultCode", 501);
        responseErroDetail.put("message", "此分類無商品");
        responseErroInfo.put("resultCode", 100);
        responseErroInfo.put("message", "Error");
        responseErroInfo.put("message", responseErroDetail);
        respInfoMap.put("responseErroInfo", responseErroInfo);
        respMap.put("Error", respInfoMap);
        String jsonString = objMapper.writeValueAsString(respMap);
        return jsonString;
    }

    public static String prodALLInfo(Clothes guClothes, List<ClothesPriceLog> clothePriceLogs) throws JsonProcessingException {
        objMapper = new ObjectMapper();
        Map<String, Object> respMap = new LinkedHashMap<>();
        Map<String, Object> respInfoMap = new LinkedHashMap<>();
        Map<String, Object> responseInfo = new LinkedHashMap<>();
        responseInfo.put("resultCode", 200);
        responseInfo.put("message", "Success");
        respInfoMap.put("responseInfo", responseInfo);
        respInfoMap.put("goods", guClothes);
        respInfoMap.put("clothesPriceLogs", clothePriceLogs);
        respMap.put("goodsInfo", respInfoMap);
        String jsonString = objMapper.writeValueAsString(respMap);
        System.out.println(jsonString);
        return jsonString;
    }

    public static String prodAllIndex(List<ClothesIndex> allProdIndex) throws JsonProcessingException {
        objMapper = new ObjectMapper();
        Map<String, Object> respMap = new LinkedHashMap<>();
        Map<String, Object> respInfoMap = new LinkedHashMap<>();
        Map<String, Object> responseInfo = new LinkedHashMap<>();
        responseInfo.put("resultCode", 200);
        responseInfo.put("message", "Success");
        respInfoMap.put("responseInfo", responseInfo);
        respInfoMap.put("allProdIndex", allProdIndex);
        respMap.put("goodsInfo", respInfoMap);
        String jsonString = objMapper.writeValueAsString(respMap);
        System.out.println(jsonString);
        return jsonString;
    }

}
