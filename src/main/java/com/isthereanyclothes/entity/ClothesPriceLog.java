package com.isthereanyclothes.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;


public class ClothesPriceLog {
    private Integer PriceLogId;

    private Long ProdNumber;

    private Integer Price;

    private Timestamp PriceRecordDate;
    
    public void setPriceLogId(Integer priceLogId) {
        PriceLogId = priceLogId;
    }

    public void setProdNumber(Long prodNumber) {
        ProdNumber = prodNumber;
    }

    public Integer getPrice() {
        return Price;
    }

    public void setPrice(Integer price) {
        Price = price;
    }

    public Timestamp getPriceRecordDate() {
        return PriceRecordDate;
    }

    public void setPriceRecordDate(Timestamp priceRecordDate) {
        PriceRecordDate = priceRecordDate;
    }
}
