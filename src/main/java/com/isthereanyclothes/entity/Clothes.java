package com.isthereanyclothes.entity;

import java.sql.Timestamp;
import java.util.List;

public class Clothes {
    private Integer Id;

    private Byte Sex;

    private String Name;

    private Long ProdNumber;

    private String About;

    private String Material;

    private Timestamp LimitedPriceDate;

    private String Category;

    private String Url;

    private String MainImageUrl;

    private String SizeUrl;

    private Boolean IsNewGood;

    private Boolean IsOnlineOnly;

    private Boolean IsSet;

    private Boolean IsLimitedTime;

    private Boolean IsPriceDown;

    private Boolean CanModify;

    private Timestamp RecordingDate;

    private Integer CurrentPrice;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Byte getSex() {
        return Sex;
    }

    public void setSex(Byte sex) {
        Sex = sex;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getProdNumber() {
        return ProdNumber;
    }

    public void setProdNumber(Long prodNumber) {
        ProdNumber = prodNumber;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String about) {
        About = about;
    }

    public String getMaterial() {
        return Material;
    }

    public void setMaterial(String material) {
        Material = material;
    }

    public Timestamp getLimitedPriceDate() {
        return LimitedPriceDate;
    }

    public void setLimitedPriceDate(Timestamp limitedPriceDate) {
        LimitedPriceDate = limitedPriceDate;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getMainImageUrl() {
        return MainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        MainImageUrl = mainImageUrl;
    }

    public String getSizeUrl() {
        return SizeUrl;
    }

    public void setSizeUrl(String sizeUrl) {
        SizeUrl = sizeUrl;
    }

    public Boolean getNewGood() {
        return IsNewGood;
    }

    public void setNewGood(Boolean newGood) {
        IsNewGood = newGood;
    }

    public Boolean getOnlineOnly() {
        return IsOnlineOnly;
    }

    public void setOnlineOnly(Boolean onlineOnly) {
        IsOnlineOnly = onlineOnly;
    }

    public Boolean getSet() {
        return IsSet;
    }

    public void setSet(Boolean isSet) {
        IsSet = isSet;
    }

    public Boolean getLimitedTime() {
        return IsLimitedTime;
    }

    public void setLimitedTime(Boolean limitedTime) {
        IsLimitedTime = limitedTime;
    }

    public Boolean getPriceDown() {
        return IsPriceDown;
    }

    public void setPriceDown(Boolean priceDown) {
        IsPriceDown = priceDown;
    }

    public Boolean getCanModify() {
        return CanModify;
    }

    public void setCanModify(Boolean canModify) {
        CanModify = canModify;
    }

    public Timestamp getRecordingDate() {
        return RecordingDate;
    }

    public void setRecordingDate(Timestamp recordingDate) {
        RecordingDate = recordingDate;
    }

    public Integer getCurrentPrice() {
        return CurrentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        CurrentPrice = currentPrice;
    }


}