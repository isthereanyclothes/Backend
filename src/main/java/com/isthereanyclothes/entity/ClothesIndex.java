package com.isthereanyclothes.entity;

import java.sql.Timestamp;

public class ClothesIndex {
    private Integer Id;

    private Byte Sex;

    private String Name;

    private Long ProdNumber;

    private Timestamp LimitedPriceDate;

    private String Category;

    private String Url;

    private String MainImageUrl;

    private Boolean IsNewGood;

    private Boolean IsOnlineOnly;

    private Boolean IsSet;

    private Boolean IsLimitedTime;

    private Boolean IsPriceDown;

    private Boolean CanModify;

    private Integer LowestPrice;

    private Integer HighestPrice;

    private Integer CurrentPrice;

    public void setId(Integer id) {
        Id = id;
    }

    public Byte getSex() {
        return Sex;
    }

    public void setSex(Byte sex) {
        Sex = sex;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getProdNumber() {
        return ProdNumber;
    }

    public void setProdNumber(Long prodNumber) {
        ProdNumber = prodNumber;
    }

    public Timestamp getLimitedPriceDate() {
        return LimitedPriceDate;
    }

    public void setLimitedPriceDate(Timestamp limitedPriceDate) {
        LimitedPriceDate = limitedPriceDate;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getMainImageUrl() {
        return MainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        MainImageUrl = mainImageUrl;
    }

    public Boolean getNewGood() {
        return IsNewGood;
    }

    public void setNewGood(Boolean newGood) {
        IsNewGood = newGood;
    }

    public Boolean getOnlineOnly() {
        return IsOnlineOnly;
    }

    public void setOnlineOnly(Boolean onlineOnly) {
        IsOnlineOnly = onlineOnly;
    }

    public Boolean getIsSet() {
        return IsSet;
    }

    public void setIsSet(Boolean isSet) {
        IsSet = isSet;
    }

    public Boolean getLimitedTime() {
        return IsLimitedTime;
    }

    public void setLimitedTime(Boolean limitedTime) {
        IsLimitedTime = limitedTime;
    }

    public Boolean getPriceDown() {
        return IsPriceDown;
    }

    public void setPriceDown(Boolean priceDown) {
        IsPriceDown = priceDown;
    }

    public Boolean getCanModify() {
        return CanModify;
    }

    public void setCanModify(Boolean canModify) {
        CanModify = canModify;
    }

    public Integer getLowestPrice() {
        return LowestPrice;
    }

    public void setLowestPrice(Integer lowestPrice) {
        LowestPrice = lowestPrice;
    }

    public Integer getHighestPrice() {
        return HighestPrice;
    }

    public void setHighestPrice(Integer highestPrice) {
        HighestPrice = highestPrice;
    }

    public Integer getCurrentPrice() {
        return CurrentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        CurrentPrice = currentPrice;
    }

}
