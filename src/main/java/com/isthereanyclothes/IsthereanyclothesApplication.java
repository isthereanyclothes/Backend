package com.isthereanyclothes;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.isthereanyclothes.controller"})
@ComponentScan(basePackages = {"com.isthereanyclothes.aop"})
@MapperScan(basePackages = {"com.isthereanyclothes.dao"})
public class IsthereanyclothesApplication {
    public static void main(String[] args) {
        SpringApplication.run(IsthereanyclothesApplication.class, args);
    }
}